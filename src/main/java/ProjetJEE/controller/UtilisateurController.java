package ProjetJEE.controller;

import javax.servlet.http.HttpSession;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import ProjetJEE.Form.ConnectionForm;
import ProjetJEE.Form.InscriptionForm;
import ProjetJEE.dao.CompteDao;
import ProjetJEE.model.Compte;
import ProjetJEE.model.Utilisateur;

@Controller
public class UtilisateurController {
	@Autowired
	private CompteDao compteDao;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String accueil() {

		return "AccueilNc";
	}

	@RequestMapping(value = "/inscription", method = RequestMethod.GET)
	public String inscription() {

		return "PageInscription";
	}

	@RequestMapping(value = "/doInscription", method = RequestMethod.POST)
	public String doInscription(HttpSession session, InscriptionForm form, Model model) {
		Utilisateur user = new Utilisateur();
		user.setAdresse(form.getAdresse());
		user.setNom(form.getNom());
		user.setPrenom(form.getPrenom());
		user.setDateDeNaissance(form.getDateDeNaissance());

		Compte compte = new Compte();
		compte.setLogin(form.getEmail());
		compte.setPassword(form.getPwd());
		compte.setUtilisateur(user);
		try {
			compte = compteDao.save(compte);
		} catch (ConstraintViolationException | DataIntegrityViolationException e) {
			model.addAttribute("messageErr","compte existe déjà");
			return "SeConnecter";
		} catch (Exception e) {
			model.addAttribute("messageErr","Impossible de crééer un compte veuillez ressayer aprés");
			return "SeConnecter";
		}
		

		session.setAttribute("connectedUser", compte);

		return "AccueilNc";
	}

	@RequestMapping(value = "/connection", method = RequestMethod.GET)
	public String connection() {
		return "SeConnecter";
	}

	@RequestMapping(value = "/doConnection", method = RequestMethod.POST)
	public String doConnection(ConnectionForm form, HttpSession session, Model model) {

		Compte compte = compteDao.findByLoginAndPassword(form.getEmail(),form.getPassword());
		
		if(compte==null) {
			model.addAttribute("messageErr","Login/mdp incorrect");
			return "SeConnecter";
		}
		
		session.setAttribute("connectedUser", compte);

		return "AccueilNc";
	}

}
