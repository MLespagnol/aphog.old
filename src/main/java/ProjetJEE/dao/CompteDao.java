package ProjetJEE.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ProjetJEE.model.Compte;

@Repository
public interface CompteDao extends JpaRepository<Compte, Long>{

	public Compte findByLoginAndPassword(String login, String password);

}
